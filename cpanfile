requires 'XML::Entities'   => '>= 1.0001';
requires 'Config::Pit'     => '>= 0.04';
requires 'Net::Amazon'     => '>= 0.61';
requires 'URI'             => '>= 1.60';
requires 'URI::Escape::XS' => '>= 0.09';
requires 'Furl'            => '>= 1.00';
requires 'JSON::XS'        => '>= 2.33';
requires 'git://github.com:nyarla/p5-Text-Nyarkdown.git';

