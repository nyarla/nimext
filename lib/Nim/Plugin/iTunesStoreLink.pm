package Nim::Plugin::iTunesStoreLink;

use strict;
use Any::Moose;

with qw/ Nim::Plugin /;

use URI;
use URI::Escape::XS qw/ uri_escape /;
use Furl;
use JSON::XS;

has country => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

has linkshare_prefix => (
    is      => 'rw',
    isa     => 'Str',
    default => q{},
);

has template => (
    is  => 'rw',
    isa => 'CodeRef',
);

has ua => (
    is         => 'rw',
    isa        => 'Object',
    lazy_build => 1,
);

sub _build_ua {
    return Furl->new(
        agent   => 'Nim::Plugin::iTunesStoreLink/0.01',
        timeout => 10,
    );
}

no Any::Moose;

sub register {
    my ( $self, $c ) = @_;

    $self->template( Nim::Plugin::iTunesStoreLink::TemplateEngine->guess($c) );

    $c->register_hook(
        $self,
        'before_entry.interpolate' => $self->can('process'),
    );
}

sub process {
    my ( $self, $c, $entry ) = @_;

    my $source = $entry->body;
       $source =~ s{iTunes:(\d+?)[\@]([a-zA-Z]+)(?:[:](.+?)[:])?}{
        my $id   = $1;
        my $type = $2;
        my $tmpl = $3 || 'iTunes';

        my $ret  = $self->interpolate( $c, $id, $type, $tmpl );

        $ret;
    }ieg;

    $entry->body( $source );
}

sub interpolate {
    my ( $self, $c, $id, $type, $tmpl ) = @_;

    $tmpl ||= 'iTunes';
    $tmpl   = "${tmpl}.html";

    $c->log->debug("Start Search iTunes Store: ${type} => ${id}");

    my $uri = URI->new('http://itunes.apple.com/lookup');
       $uri->query_form(
           id      => $id,
           country => $self->country,
           entity  => $type,
        );

    my $res = $self->ua->get("${uri}");

    if ( ! $res->is_success ) {
        $c->log->warn("Search iTunes Store is failed: ${type} => ${id}");
        return q{};
    }
    else {
        $c->log->debug("Finished Search iTunes Store: ${type} => ${id}");
    }

    my $json = decode_json $res->body;
    my $data = $json->{'results'}->[0];
    
    if ( $self->linkshare_prefix ne q{} ) {
        my $url = URI->new($data->{'trackViewUrl'});
           $url->query_form({});
           $url->query_form( 'partnerId' => 30);

        my $link = $self->linkshare_prefix . uri_escape( uri_escape("${url}") );

        $data->{'Permalink'} = $link;
    }
    else {
        $data->{'Permalink'} = $data->{'trackViewUrl'};
    }

    my $ret  = $self->template->( $tmpl, $data );

    return $ret;
}

package
    Nim::Plugin::iTunesStoreLink::TemplateEngine;

sub guess {
    my ( $class, $context ) = @_;
    my $source = $context->hooks;

    for my $name (qw( entry.interpolate page.interpolate )) {
        if ( exists $source->{$name} ) {
            my $hooks = $source->{$name};
            for my $hook ( @{ $hooks } ) {
                if ( ref $hook->{'plugin'} eq 'Nim::Plugin::Template::MicroTemplate' ) {
                    return sub {
                        my ( $tmpl, @vars ) = @_;
                        $hook->{'plugin'}->mt->render($tmpl, @vars);
                    };
                }
            }
        }
    }

    die "Cannot find template plugin.";
}

1;

package Nim::Plugin::iTunesStoreLink;

__PACKAGE__->meta->make_immutable;

1;

