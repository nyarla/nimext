package Nim::Plugin::DateSection;

use strict;
use Any::Moose;

with 'Nim::Plugin';

no Any::Moose;

sub register {
    my ( $self, $c ) = @_;
    
    $c->register_hook(
        $self,
        'after_find_entries' => $self->can('apply'),
    );
}

sub apply {
    my ( $self, $c ) = @_;

    my %section = (
        # {YYYY}-{MM}-{DD} => {count},
    );

    for my $entry ( sort { $a->datetime <=> $b->datetime } @{ $c->entries } ) {
        my $date = $entry->datetime->ymd('-');
        
        $section{$date} = 0 if ( ! defined $section{$date} );
        $section{$date}++;

        $entry->meta->{'datesection'} = $section{$date};
    }
}

__PACKAGE__->meta->make_immutable;