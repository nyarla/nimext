package Nim::Plugin::Entry::DayOne;

use strict;
use Any::Moose;
use Carp;

with qw/ Nim::Plugin /;

use XML::Entities;
use DateTime::Format::W3CDTF;

use Nim::Types;
use Nim::Entry;

has journal => (
    is       => 'rw',
    isa      => 'Nim::Types::Dir',
    coerce   => 1,
    required => 1,
    default  => sub {
        join q{/}, ( $ENV{'HOME'}, qw/ Dropbox Journal.dayone entries / )
    },
);

has set_meta => (
    is      => 'rw',
    isa     => 'Bool',
    default => sub { ! 1 },
);

has meta_options => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { +{ style => 'meta' } },
);

has use_markdown => (
    is      => 'rw',
    isa     => 'Bool',
    default => sub { !! 1 },
);

no Any::Moose;

sub register {
    my ( $self, $c ) = @_;
    $c->register_hook(
        $self,
        find_entries => $self->can('find'),
    );
}

sub find {
    my ( $self, $c ) = @_;

    my $dir = $self->journal;
    $dir->recurse( callback => sub {
        my $file = $_[0];

        return unless -f $file;
        return unless $file->basename =~ m{\.doentry$};

        my $fh   = $file->open('<:encoding(utf8)')
            or Carp::confess "Cannot open dayone file: ${file}: ${!}";

        my $plist   = $self->parse_plist( do { local $/; <$fh> } );
       
        my $w3cdtf  = $plist->{'date'};
        my $content = $plist->{'text'};
        my $id      = $plist->{'uuid'};
        my $starred = $plist->{'star'};

        my $dt      = DateTime::Format::W3CDTF->parse_datetime($w3cdtf);
           $dt->set_time_zone( $c->conf->time_zone );
        my $time    = $dt->epoch;

        my $entry = Nim::Entry->new(
            context  => $c,
            path     => '',
            filename => $id,
            time     => $time,
            datetime => $dt,
            loader   => sub {
                my ( $entry, $want ) = @_;

                my $body = $content;
                   # $body =~ s{\x0D\x0A|\x0D|\x0A}{\n}sg;

                if ( $self->set_meta ) {
                    my $options = $self->meta_options;
                    my $style   = $options->{'style'} || 'meta';
                    my $header  = q{};
                    my $markup  = ( $self->use_markdown ) ? 'markdown' : 'text';

                    if ( $style eq 'meta' ) { 
                        $header .= "uuid: ${id}\n",
                        $header .= "starred: ${starred}\n";
                        $header .= "markup: ${markup}\n";
                    }
                    elsif ( $style eq 'plasxom' ) {
                        my $prefix = $options->{'prefix'} || '@';
                        
                        $header .= "${prefix}uuid: ${id}\n";
                        $header .= "${prefix}starred: ${starred}\n";
                        $header .= "${prefix}markup: ${markup}\n";
                    }
                    else {
                        $c->log->error('Unsupport meta style: "%s"', $style);
                    }

                    if ( $header ne q{} ) {
                        $body = "${header}\n\n${body}";
                    }
                }

                return ( $want eq 'title' ) ? q{} : $body ;
            },
        );
        
        push @{ $c->entries }, $entry;
    } );

    return 1;
}

sub parse_plist {
    my ( $self, $plist ) = @_;

    my ( $date ) = ( $plist =~ m{<key>Creation Date</key>\s*<date>(.+?)</date>\s*}s );
    my ( $text ) = ( $plist =~ m{<key>Entry Text</key>\s*<string>([\s\S]+?)</string>\s*}s );
    my ( $starred ) = ( $plist =~ m{<key>Starred</key>\s*<(true|false)/>\s*}s );
    my ( $UUID ) = ( $plist =~ m{<key>UUID</key>\s*<string>(.+?)</string>\s*}s );

    $text = XML::Entities::decode( all => $text );

    return {
        date => $date,
        text => $text,
        star => $starred,
        uuid => $UUID,
    };
}

__PACKAGE__->meta->make_immutable;
