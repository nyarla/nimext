package Nim::Plugin::Entry::IYHMemo;

use strict;
use Any::Moose;

extends qw/ Nim::Plugin::Entry::Clmemo /;

use Config::Pit;
use Net::Amazon;
use DateTime::Format::W3CDTF;

has net_amazon => (
    is          => 'rw',
    isa         => 'Net::Amazon',
    lazy_build  => 1,
);

has locale => (
    is          => 'rw',
    isa         => 'Str',
    required    => 1,
);

has asoid => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

has use_cache => (
    is  => 'rw',
    isa => 'Bool',
);

has cache => (
    is => 'rw',
    isa => 'HashRef',
);

has markup => (
    is      => 'rw',
    isa     => 'Str',
);

sub _build_net_amazon {
    my ( $self ) = @_;

    my $pit = pit_get('amazonaws', require => {
        'apikey'    => 'Your AWS APIKey:',
        'secret'    => 'Your AWS Secret key:',
    });

    my $amzargs = {
        token       => $pit->{'apikey'},
        secret_key  => $pit->{'secret'},
        associate_tag => $self->asoid,
    };

    if ( !! $self->use_cache && exists $self->cache->{'class'} ) {
        my $config  = $self->cache;
        my $class   = $config->{'class'};
        my $args    = $config->{'args'};
        my $deref   = !! $config->{'deref'};
        
        Any::Moose::load_class($class)
            if ( ! Any::Moose::is_class_loaded($class) );
        
        my @args;
        if ( $deref ) {
            if ( ref $args eq 'ARRAY' ) {
                @args = @{ $args }
            }
            elsif ( ref $args eq 'HASH' ) {
                @args = %{ $args }
            }
            else {
                die "Unsupport reference: ${args}";
            }
        }
        else {
            push @args, $args;
        }
        
        $amzargs->{'cache'} = $class->new(@args);
    }

    $amzargs->{'locale'} = $self->locale;

    return Net::Amazon->new(%{ $amzargs });
}

no Any::Moose;

sub find {
    my ( $self, $c ) = @_;

    my $memo = Path::Class::File->new( $self->file );
    die qq{Cannot find iyhmemo file: ${memo}}
        if ( ! -f $memo );

    open( my $fh, '<' . $self->open_layer, $memo );

    my @cache = ();
    my ( $parsed, $added );
    my ( $datetime, $asin, $body, @tags );

    while ( my $line = <$fh> ) {
        # date header
        if ( $line =~ m{^[#]} ) {
            if ( $parsed ) {
                push @cache, $self->mk_entry_source( $c, $datetime, $asin, $body, [ @tags ]);
                $asin  = undef;
                $body  = undef;
                @tags   = ();
            }
            
            $parsed = 0;
            
            # add entries
            if ( @cache ) {
                $self->add_entries( $c, @cache );
                $added += scalar( @cache );
                @cache = ();
                last if ( $self->limit && $added >= $self->limit );
            }
            
            # set date
            my ( $datestr ) = ( $line =~ m{^[#]\s*(.+?)\s*$} );
               $datestr =~ s{^\s*|\s*$}{}g;
            
            $datetime = DateTime::Format::W3CDTF->parse_datetime( $datestr );
        }
        # item header
        elsif ( $line =~ m{^[-]{3}\s*(.+)\s*$} ) {
            my $data = $1;
            my $tags = q{};
            
            if ( $parsed ) {
                push @cache, $self->mk_entry_source( $c, $datetime, $asin, $body, [ @tags ] );
                $asin   = undef;
                $body   = undef;
                @tags   = ();
            }
            
            $parsed++;
            
            # ASIN [ @tags ]
            if ( $data =~ m{^([0-9A-Z]{10})\s*\[(.+?)\]} ) {
                $asin   = $1;
                $tags   = $2;
            }
            # ASIN
            elsif ( $data =~ m{^([0-9A-Z]{10})\s*$} ) {
                $asin = $1;
            }
            # [ @tags ]
            elsif ( $data =~ m{^\[(.+?)\]} ) {
                $tags = $1;
            }
            # no meta data
            else {}
            
            if ( $tags ne q{} ) {
                ( @tags ) = split m{\s*[,]\s*}, $tags;
            }
        }
        # body
        else {
            $body .= $line;
        }
    }
    
    if ( $parsed ) {
        push @cache, $self->mk_entry_source( $c, $datetime, $asin, $body, [ @tags ] );
    }
    
    if ( @cache ) {
        $self->add_entries( $c, @cache );
    }
    
    close($fh);
}

sub search_asin {
    my ( $self, $c, $asin ) = @_;

    $c->log->info("Start Search ASIN: ${asin}");
    
    my $result = $self->net_amazon->search( asin => $asin );
    
    if ( $result->is_success ) {
        $c->log->info("Search ASIN is successed: ${asin}");
        return ( $result->properties )[0];
    }
    else {
        $c->log->warn("Search ASIN is failed: ${asin}: @{[ $result->message ]}");
        return undef;
    }
}

sub mk_entry_source {
    my ( $self, $c, $datetime, $asin, $body, $tags ) = @_;

    my $item    = undef;
       $item    = $self->search_asin( $c, $asin ) if ( defined $asin && $asin ne q{} );
    my $title   = ( defined $item ) ? $item->ProductName : q{no title} ;
    
    return {
        date    => $datetime,
        title   => $title,
        tags    => $tags,
        body    => $body,
        asin => $asin,
        item => $item,
    };
}

sub add_entries {
    my ( $self, $c, @day_entries ) = @_;

    my @entries = ();
    my $id      = 0;
    
    for my $info ( reverse @day_entries ) {
        $info->{'title'} =~ s{\^s*|\s*$}{}g;
        
        push @entries, Nim::Entry->new(
            context     => $c,
            path        => join( q{/}, $self->path, $info->{'date'}->ymd('/') ),
            filename    => ++$id,
            time        => $info->{'date'}->epoch,
            datetime    => $info->{'date'},
            loader      => sub {
                my ( $entry, $want ) = @_;
                
                $entry->title( $info->{'title'} );
                $entry->body( $info->{'body'} );
                
                return ( $want eq 'title' ) ? $info->{'title'} : $info->{'body'};
            },
            meta        => {
                tags    => $info->{'tags'},
                asin    => $info->{'asin'},
                item    => $info->{'item'},
                ( defined $self->markup ?  ( markup => $self->markup ) : () ),
            },
        );
    }
    
    push @{ $c->entries }, reverse @entries;
}



__PACKAGE__->meta->make_immutable;

