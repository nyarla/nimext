package Nim::Plugin::PlasxomMeta;

use strict;
use Any::Moose;

with 'Nim::Plugin';

use DateTime::Format::W3CDTF;

has meta_prefix => (
    is      => 'rw',
    isa     => 'Str',
    default => '@',
);

has meta_date => (
    is      => 'rw',
    isa     => 'Str',
    default => 'date',
);

has meta_tags => (
    is => 'rw',
    isa => 'Str',
    default => 'tags',
);

no Any::Moose;

{
    package Nim::Plugin::PlasxomMeta::Entry;
    
    use strict;
    use Any::Moose '::Role';
    
    has meta => (
        is      => 'rw',
        isa     => 'HashRef',
        default => sub { +{} },
    );
}


sub register {
    my ( $self, $c ) = @_;

    $c->register_hook(
        $self,
        'after_find_entries' => $self->can('apply'),
    );

    my $meta = Nim::Entry->meta;

    $meta->meta_mutable if ( $Any::Moose::PREFERRED eq 'Moose' );
    Nim::Plugin::PlasxomMeta::Entry->meta->apply( $meta );
    $meta->make_immutable;
}

sub apply {
    my ( $self, $c ) = @_;

    for my $entry ( @{ $c->entries } ) {
        my $source  = $entry->body;
        my $meta    = ( ref $entry->{meta} eq 'HASH' ) ? $entry->{meta} : {};
        my $body    = q{};

        # parse meta
        my $prefix  = quotemeta( $self->meta_prefix );
        my @lines = split m{\r?\n}, $source;

        while ( my $line = shift @lines ) {
            chomp($line);
            if ( $line !~ m{^$prefix} ) {
                unshift @lines, $line;
                last;
            }
            
            if ( $line =~ m{^$prefix([a-zA-Z0-9]+?)[:]\s*(.*?)\s*$} ) {
                my $key     = $1;
                my $value   = $2;
                
                $meta->{$key} = $value;
            }
        }
        
        $body = join qq{\n}, @lines;

        my $date_tag = $self->meta_date;
        if ( $meta->{$date_tag} ) {
            my $dt = DateTime::Format::W3CDTF->parse_datetime( $meta->{$date_tag} );
            $entry->time( $dt->epoch );
        }

        my $tags_tag = $self->meta_tags;
        if ( exists $meta->{$tags_tag} && ! ref $meta->{$tags_tag} ) {
            my @tags = split qr{\s*,\s*}, $meta->{$tags_tag};
            $meta->{$tags_tag} = [ @tags ];
        }

        $entry->meta( $meta );
        $entry->body( $body );
    }

}

__PACKAGE__->meta->make_immutable;
