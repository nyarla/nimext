package Nim::Plugin::ReplaceASIN;

use strict;
use Any::Moose;

with 'Nim::Plugin';

use Config::Pit;
use Net::Amazon;

has template => (
    is          => 'rw',
    isa         => 'CodeRef',
);

has net_amazon => (
    is          => 'rw',
    isa         => 'Net::Amazon',
    lazy_build  => 1,
);

has locale => (
    is          => 'rw',
    isa         => 'Str',
    required    => 1,
);

has asoid => (
    is  => 'rw',
    isa => 'Str',
    required => 1,
);

has use_cache => (
    is  => 'rw',
    isa => 'Bool',
);

has cache => (
    is => 'rw',
    isa => 'HashRef',
);

has __cache => (
    is         => 'ro',
    isa        => 'Object',
    lazy_build => 1,
);

sub _build___cache {
    my $self = shift;
    my $config  = $self->cache;
    my $class   = $config->{'class'};
    my $args    = $config->{'args'};
    my $deref   = !! $config->{'deref'};
    
    Any::Moose::load_class($class)
        if ( ! Any::Moose::is_class_loaded($class) );
    
    my @args;
    if ( $deref ) {
        if ( ref $args eq 'ARRAY' ) {
            @args = @{ $args }
        }
        elsif ( ref $args eq 'HASH' ) {
            @args = %{ $args }
        }
        else {
            die "Unsupport reference: ${args}";
        }
    }
    else {
        push @args, $args;
    }
    
    return $class->new(@args);
}

sub _build_net_amazon {
    my ( $self ) = @_;

    my $pit = pit_get('amazonaws', require => {
        'apikey'    => 'Your AWS APIKey:',
        'secret'    => 'Your AWS Secret key:',
    });

    my $amzargs = {
        token       => $pit->{'apikey'},
        secret_key  => $pit->{'secret'},
        associate_tag => $self->asoid,
    };

    if ( !! $self->use_cache && defined( my $cache = $self->__cache ) ) {
        $amzargs->{'cache'} = $cache;
    }

    $amzargs->{'locale'} = $self->locale;

    return Net::Amazon->new(%{ $amzargs });
}

no Any::Moose;

sub register {
    my ( $self, $c ) = @_;

    $self->template( Nim::Plugin::ReplaceASIN::TemplateEngine->guess($c) );
    
    $c->register_hook(
        $self,
        'before_entry.interpolate' => $self->can('process'),
    );

}

sub process {
    my ( $self, $c, $entry ) = @_;

    my $source = $entry->body;
       $source =~ s{ASIN:([A-Z0-9]{10})(?:[:](.*?)[:])?}{
            my $asin = $1;
            my $tmpl = $2 || 'asin';

            my $ret  = $self->interpolate($c, $asin, $tmpl);

            $ret;
       }ieg;

    $entry->body( $source );
}

sub interpolate {
    my ( $self, $c, $asin, $tmpl ) = @_;

    $tmpl ||= 'asin';
    $tmpl = "${tmpl}.html";


    $c->log->debug("Start search ASIN: ${asin}");
    my $response = $self->net_amazon->search( asin => $asin );

    if ( ! $response->is_success ) {
        $c->log->warn("Search ASIN is failed: ${asin}");
        return q{};
    }
    else {
        $c->log->debug("Finished search ASIN: ${asin}");
    }

    my $result = $self->template->( $tmpl, $response );

    return $result;
}

__PACKAGE__->meta->make_immutable;

package Nim::Plugin::ReplaceASIN::TemplateEngine;

use strict;
use warnings;

sub guess {
    my ( $class, $context ) = @_;
    my $source = $context->hooks;

    for my $name (qw( entry.interpolate page.interpolate )) {
        if ( exists $source->{$name} ) {
            my $hooks = $source->{$name};
            for my $hook ( @{ $hooks } ) {
                if ( ref $hook->{'plugin'} eq 'Nim::Plugin::Template::MicroTemplate' ) {
                    return sub {
                        my ( $tmpl, @vars ) = @_;
                        $hook->{'plugin'}->mt->render($tmpl, @vars);
                    };
                }
            }
        }
    }

    die "Cannot find template plugin.";
}

1;

package Nim::Plugin::ReplaceASIN;

1;
