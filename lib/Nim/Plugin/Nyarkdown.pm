package Nim::Plugin::Nyarkdown;

use strict;
use Any::Moose;

with qw/ Nim::Plugin /;

use Encode;
use Text::Nyarkdown;

has nd => (
    is         => 'rw',
    isa        => 'Text::Nyarkdown',
    lazy_build => 1,
    handles    => [qw/markdown/],
);

has options => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { +{} },
);

has enable_embed => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub { +[] },
);

no Any::Moose;

sub register {
    my ( $self, $c ) = @_;

    $c->register_hook(
        $self,
        'before_entry.interpolate' => $self->can('process'),
    );
}

sub process {
    my ( $self, $c, $entry ) = @_;

    if ( $entry->can('meta') and my $markup = $entry->meta->{'markup'} ) {
        return unless lc $markup eq 'markdown';
    }

    $c->log->debug('apply markdown filter to %s', join('/', $entry->path, $entry->filename));

    my $body = $self->markdown( encode_utf8($entry->body) => {
        enable_embed => $self->enable_embed,
    });
    $entry->body( decode_utf8 $body );
}

sub _build_nd {
    my $nd = Text::Nyarkdown->new(%{ $_[0]->options });
    return $nd;
}

__PACKAGE__->meta->make_immutable;

